#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 14:04:01 2017

@author: dimarenich
"""

import unicodecsv
from os.path import basename, splitext


def read_csv(file):
    with open(file, 'rb') as f:
        reader = unicodecsv.DictReader(f)
        return list(reader)


def extract_filename(file):
    filename = str(splitext(basename(file))[0])
    return filename


def analyze_csv(file):
    if file == '':
        return None
    else:
        def num_rows(file):
            data = read_csv(file)
            name = extract_filename(file)
            num_rows = len(data)
            print(name + '_num_rows = ' + str(num_rows))
            return

        def num_unique_students(file):
            data = read_csv(file)
            uniq = set()
            for elem in data:
                uniq.add(elem['account_key'])
            name = extract_filename(file)
            num_uniq = len(uniq)
            print(name + '_num_unique_students = ' + str(num_uniq) + '\n')
            return
        num_rows(file)
        num_unique_students(file)
    return


enrollments = 'datasets/enrollments.csv'
daily_engagement = 'datasets/daily_engagement.csv'
project_submissions = 'datasets/project_submissions.csv'

analyze_csv(enrollments)
analyze_csv(project_submissions)
# analyze_csv(daily_engagement)
